﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EGApplication : MonoBehaviour {

    public GameObject Player { get { return GameObject.FindGameObjectWithTag("Player"); } }
    public Camera Camera { get { return GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>(); } }
    public GameObject Terrain { get { return GameObject.FindGameObjectWithTag("Terrain"); } }
    public EGPrefabLibrary Prefabs { get { return transform.Find("PrefabLibrary").gameObject.GetComponent<EGPrefabLibrary>(); } }
    public EGMaterialLibrary Materials { get { return transform.Find("MaterialLibrary").gameObject.GetComponent<EGMaterialLibrary>(); } }

    private EGBuilding PrefabInHand { get; set; }
    private GameObject PrefabModel { get { return this.PrefabInHand.gameObject.transform.Find("model").gameObject; } }
    private GameObject PrefabMarker { get { return this.PrefabInHand.gameObject.transform.Find("marker").gameObject; } }
    private Vector3 HitNormal { get; set; }
    private GameObject HitObject { get; set; }
    private Vector3 HitPosition { get; set; }
    private float LastBuildingRotation { get; set; }

    private Dictionary<Vector3, EGBuilding> ObjectsOnStage { get; set; }

    private bool AllowBuilding { get; set; }

    //Взять в руки объект
    public void PickupObject(GameObject gameObject)
    {
        this.ThrowObject();

        this.PrefabInHand = Instantiate(gameObject).GetComponent<EGBuilding>();
        GameObject marker = Instantiate(this.PrefabInHand.Model);
        marker.transform.SetParent(this.PrefabInHand.gameObject.transform);
        marker.name = "marker";
        marker.transform.localPosition = Vector3.zero;
        this.PrefabInHand.gameObject.layer = 2;
        this.PrefabInHand.Rotation = this.LastBuildingRotation;
        marker.SetActive(true);
    }

    //Выбросить объект
    public void ThrowObject()
    {
        if(this.PrefabInHand != null) Destroy(this.PrefabInHand.gameObject);
    }

    //Разместить объект
    public void SetObject()
    {
        if (!this.AllowBuilding) return;
        GameObject objectClone = Input.GetKey(KeyCode.LeftShift)? Instantiate(this.PrefabInHand.gameObject):null;
                
        Destroy(this.PrefabMarker);
        this.PrefabInHand.gameObject.layer = 10;
        this.PrefabInHand.Builded = true;
        this.ObjectsOnStage.Add(this.PrefabInHand.Position, this.PrefabInHand);
        this.PrefabModel.SetActive(true);
        this.PrefabInHand = (objectClone != null)?objectClone.GetComponent<EGBuilding>():null;
    }

    private void ChangeMarkerMaterial(GameObject gameObject, Material material)
    {
        MeshRenderer renderer = gameObject.GetComponent<MeshRenderer>();
        if(renderer != null)
        {
            renderer.materials = new Material[] { };
            renderer.material = material;
        }
        for (int i = 0; i < gameObject.transform.childCount; i++) this.ChangeMarkerMaterial(gameObject.transform.GetChild(i).gameObject, material);
    }

    private void ChangeMarkerMaterial(Material material)
    {
        this.ChangeMarkerMaterial(this.PrefabMarker, material);
    }

    private void OnDrawGizmos()
    {
        for (int i = -100; i < 100; i++)
        {
            Gizmos.DrawLine(
                new Vector3(-100.0f, 0.0f, (float)i),
                new Vector3(100.0f, 0.0f, (float)i)
                );
            Gizmos.DrawLine(
                new Vector3((float)i, 0.0f, -100.0f),
                new Vector3((float)i, 0.0f, 100.0f)
                );
        }
    }

    private void GetHitParams()
    {
        //Vector3 mousePosition = new Vector3(Screen.width/2, Screen.height/2);
        Vector3 mousePosition = Input.mousePosition;

        if (mousePosition.y <= 70.0f) mousePosition.y = 70.0f;

        Ray ray = this.Camera.ScreenPointToRay(mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            this.HitObject = hit.collider.gameObject;
            this.HitNormal = hit.normal;
            this.HitPosition = hit.point;
        }
        else
        {
            this.HitObject = null;
        }
    }

    private void PrefabInHandHandler()
    {
        if (this.PrefabInHand == null) return;
        
        bool seeOnTerrain = (this.HitObject != null && this.HitObject.tag == "Terrain") ? true : false;
        bool seeOnConstruction = (this.HitObject != null && this.HitObject.GetComponent<EGConstruction>() != null) ? true : false;

        if (!seeOnTerrain && !seeOnConstruction) this.AllowBuilding = false;

        //Если смтрим на землю
        if (seeOnTerrain)
        {
            //Если объект можно устанавливать на землю
            if (this.PrefabInHand.AllowBuildToTerrain)
            {
                this.AllowBuilding = true;
            }
            else
            {
                this.AllowBuilding = false;
            }
        }

        //Если смотрим на конструкцию
        if (seeOnConstruction)
        {
            if (this.PrefabInHand.ObjectID == "000_Hammer" && Input.GetMouseButtonDown(0))
            {
                Destroy(this.HitObject);
            }

            //Если объект можно устанавливать на конструкции
            if (this.PrefabInHand.AllowBuildToConstructions && this.HitObject.GetComponent<EGConstruction>() != null && this.HitObject.GetComponent<EGConstruction>().AllowInputBuild)
            {
                if(this.HitNormal == Vector3.up)
                {
                    this.PrefabInHand.Position = new Vector3(this.PrefabInHand.Position.x, this.HitPosition.y, this.PrefabInHand.Position.z);
                    this.AllowBuilding = true;
                }
                else
                {
                    this.AllowBuilding = false;
                }                
            }
            else
            {
                this.AllowBuilding = false;
            }

            //Если объект можно прицеплять к конструкциям
            if (((EGConstruction)this.PrefabInHand).AllowConnectToConstructions && this.HitObject.GetComponent<EGConstruction>() != null && this.HitObject.GetComponent<EGConstruction>().AllowInputConnect)
            {
                //Если объект можно прицеплять только к эквивалентным объектам
                if (((EGConstruction)this.PrefabInHand).AllowEqualsConnection && this.PrefabInHand.ObjectID != this.HitObject.GetComponent<EGConstruction>().ObjectID)
                {
                    this.AllowBuilding = false;
                }
                else
                {

                    if (this.HitNormal == Vector3.forward)
                    {
                        this.PrefabInHand.Position = new Vector3(this.PrefabInHand.Position.x, this.PrefabInHand.Position.y, this.HitObject.transform.position.z + 1.0f);
                        this.PrefabInHand.Rotation = 270.0f;
                        this.AllowBuilding = true;
                    }
                    else if (this.HitNormal == Vector3.right)
                    {
                        this.PrefabInHand.Position = new Vector3(this.HitObject.transform.position.x + 1.0f, this.PrefabInHand.Position.y, this.PrefabInHand.Position.z);
                        this.PrefabInHand.Rotation = 0.0f;
                        this.AllowBuilding = true;
                    }
                    else if (this.HitNormal == -Vector3.right)
                    {
                        this.PrefabInHand.Position = new Vector3(this.HitObject.transform.position.x - 1.0f, this.PrefabInHand.Position.y, this.PrefabInHand.Position.z);
                        this.PrefabInHand.Rotation = 180.0f;
                        this.AllowBuilding = true;
                    }
                    else if (this.HitNormal == -Vector3.forward)
                    {
                        this.PrefabInHand.Position = new Vector3(this.PrefabInHand.Position.x, this.PrefabInHand.Position.y, this.HitObject.transform.position.z - 1.0f);
                        this.PrefabInHand.Rotation = 90.0f;
                        this.AllowBuilding = true;
                    }
                    else
                    {
                        this.AllowBuilding = false;
                    }
                }
            }
        }

        if (this.ObjectsOnStage.ContainsKey(this.PrefabInHand.Position))
        {
            this.AllowBuilding = false;
        }

        if((this.PrefabInHand.Position - this.Player.transform.position).magnitude > 5.0f)
        {
            this.AllowBuilding = false;
        }
    }

    private void BuildingHandler()
    {
        if (this.PrefabInHand == null) return;
        
        if (this.AllowBuilding) this.ChangeMarkerMaterial(this.Materials.HologramGreen);
        else this.ChangeMarkerMaterial(this.Materials.HologramRed);

        if (Input.GetMouseButtonDown(0) && Input.mousePosition.y > 70.0f)
        {
            //Если нажата левая кнопка мыши - пытаемся установить объект
            this.SetObject();
        }

        if (Input.GetKey(KeyCode.Escape))
        {
            this.ThrowObject();
        }        
    }

    private void PositionHandler()
    {
        if (this.PrefabInHand == null || this.HitObject == null) return;

        Vector3 position = this.HitPosition;
        if (position.x < 0.0f) position.x -= 1.0f;
        if (position.z < 0.0f) position.z -= 1.0f;
        this.PrefabInHand.Position = new Vector3((int)position.x + .5f, (int)position.y, (int)position.z + .5f);

        if (Input.GetKeyDown(KeyCode.R))
        {
            this.PrefabInHand.Rotation = this.PrefabInHand.Rotation == 270.0f ? 0.0f : this.PrefabInHand.Rotation + 90.0f;
            this.LastBuildingRotation = this.PrefabInHand.Rotation;
        }
    }

    private void Awake()
    {
        this.ObjectsOnStage = new Dictionary<Vector3, EGBuilding>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            this.PickupObject(this.Prefabs.ArmedPillar001);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            this.PickupObject(this.Prefabs.SteelPillar002);
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            this.PickupObject(this.Prefabs.SupportBalk003);
        }

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            this.PickupObject(this.Prefabs.SupportPlatform004);
        }

        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            this.PickupObject(this.Prefabs.SteelStair005);
        }

        this.GetHitParams();
        this.PositionHandler();
        this.PrefabInHandHandler();
        this.BuildingHandler();        
    }

    private void OnGUI()
    {
        if (this.HitObject != null)
        {
            GUI.Label(new Rect(10, 10, 500, 20), this.HitObject.name);
            GUI.Label(new Rect(10, 30, 500, 20), this.HitNormal.ToString());
            GUI.Label(new Rect(10, 50, 500, 20), this.HitPosition.ToString());
        }
    }
}
