﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EGPrefabLibrary : MonoBehaviour {

    public GameObject ArmedPillar001;
    public GameObject SteelPillar002;
    public GameObject SupportBalk003;
    public GameObject SupportPlatform004;
    public GameObject SteelStair005;
    public GameObject Destroyer;

}
