﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object001MineBehaviour : MonoBehaviour {

    public enum ObjectDirection { North = 0, East = 1, South = 2, West = 3}

    public ApplicationScript App { get { return GameObject.FindGameObjectWithTag("Application").GetComponent<ApplicationScript>(); }}

    public GameObject Model;
    public GameObject Marker;
    public GameObject IndicatorBlue;
    public GameObject IndicatorYellow;

    public Animator Animator { get { return this.Model.GetComponent<Animator>(); }}
    public Vector3 PowerConnectorPosition { get { return IndicatorBlue.transform.position; }}

    public Material GreenMaterial;
    public Material RedMaterial;
    public Material GrayMaterial;

    public bool IsInstalled;
    public bool IsPower;
    public bool IsSender;
    public bool IsCollision;
    public bool IsFar;

    [Range(0.0f, 4500.0f)]
    public float Power;
    public float MinPower = 200.0f;
    public float MaxPower = 4000.0f;

    public ObjectDirection Direction;

    public void Install(){
        this.IsInstalled = true;
        this.gameObject.layer = 10;
        Debug.Log(this.PowerConnectorPosition);
    }

    private void ChangeMarkerMaterial(Material material, GameObject gameObject)
    {
        MeshRenderer renderer = gameObject.GetComponent<MeshRenderer>();
        if (renderer != null) renderer.material = material;
        for(int i = 0; i < gameObject.transform.childCount; i++)
        {
            this.ChangeMarkerMaterial(material, gameObject.transform.GetChild(i).gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        this.IsCollision = true;
    }

    private void OnCollisionExit(Collision collision)
    {
        this.IsCollision = false;
    }

    private void Update(){
        if(!IsInstalled){
            Model.SetActive(false);
            Marker.SetActive(true);
            IndicatorBlue.SetActive(false);
            IndicatorYellow.SetActive(false);

            float dist;
            Plane plane = new Plane(App.Terrain.transform.up, App.Terrain.transform.position);

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (plane.Raycast(ray, out dist)){
                Vector3 position = ray.GetPoint(dist);
                if(this.Direction == ObjectDirection.East || this.Direction == ObjectDirection.West)
                    this.transform.position = new Vector3((float)(int)position.x + .5f, 0.0f, (float)(int)position.z);
                else
                    this.transform.position = new Vector3((float)(int)position.x, 0.0f, (float)(int)position.z + .5f);
            }

            if (Input.GetKeyDown(KeyCode.R)){
                //this.transform.Rotate(Vector3.up, 90.0f);
                int enumLength = System.Enum.GetValues(typeof(ObjectDirection)).Length;
                int nextIndex = (int)this.Direction + 1;

                if(nextIndex >= enumLength) nextIndex = 0;

                this.Direction = (ObjectDirection)nextIndex;
            }

            this.transform.rotation = Quaternion.Euler(0.0f, 90.0f * (float)this.Direction, 0.0f);

            if((this.transform.position - App.Player.transform.position).magnitude > 5.0f){
                IsFar = true;
            }else{
                IsFar = false;
            }

            if(IsFar){
                this.ChangeMarkerMaterial(this.GrayMaterial, this.Marker);
            }else{
                if (IsCollision)
                {
                    this.ChangeMarkerMaterial(this.RedMaterial, this.Marker);
                }
                else
                {
                    this.ChangeMarkerMaterial(this.GreenMaterial, this.Marker);
                    if (Input.GetMouseButtonDown(0)) this.Install();
                }
            }



            
        }
        else{
            Model.SetActive(true);
            Marker.SetActive(false);

            IndicatorYellow.SetActive(!IsPower);
            IndicatorBlue.SetActive(!IsSender);

            if(this.IsPower && (this.Power >= this.MinPower && this.Power <= this.MaxPower)) this.Animator.speed = this.Power / this.MaxPower * 2.0f;
            else this.Animator.speed = 0.0f;
        }
	}
}
