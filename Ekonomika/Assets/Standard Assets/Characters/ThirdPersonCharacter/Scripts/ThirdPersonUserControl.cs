using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class ThirdPersonUserControl : MonoBehaviour
    {
        private ThirdPersonCharacter Character;
        private Transform CharacterTransform { get; set; }
        private Transform CameraTransform { get; set; }
        private Camera Camera { get; set; }
        private Vector3 VectorMove;
        private Vector3 ClickMousePosition;
        private bool IsJump;
        private float RotateCounter;

        private Vector3 LastFrameMousePosition { get; set; }  

        private void Awake(){
            Character = GetComponent<ThirdPersonCharacter>();
            CharacterTransform = transform;
            CameraTransform = Camera.main.transform;
            Camera = Camera.main;
        }

        private void Start(){
            
        }


        private void Update(){
            
        }


        private void FixedUpdate(){

            RaycastHit hit;
            if(Physics.Raycast(Camera.ScreenPointToRay(Input.mousePosition), out hit))
            {
                if(hit.collider.gameObject.name == "Terrain")
                {

                }
            }

            //if (!IsJump) IsJump = CrossPlatformInputManager.GetButtonDown("Jump");
            if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)) this.ClickMousePosition = Input.mousePosition;
            if (Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1)) this.ClickMousePosition = Vector3.zero;

            Vector3 currentMousePosition = Input.mousePosition;
            Vector3 deltaPosition = this.ClickMousePosition - currentMousePosition;
            Vector3 deltaFramePosition = this.LastFrameMousePosition - currentMousePosition;

            //��� �� ����
            Ray ray = Camera.ScreenPointToRay(new Vector2(Screen.width / 2, Screen.height / 2));
            float zoomDistance = 100f * Time.deltaTime;
            float zoomAxis = 0.0f;
            float inputAxis = Input.GetAxis("Mouse ScrollWheel");
            float altitude = CameraTransform.position.y;

            if (altitude < 4.6f) zoomAxis = inputAxis < 0.0f ? inputAxis : 0.0f;
            else if (altitude > 10.7f) zoomAxis = inputAxis > 0.0f ? inputAxis : 0.0f;
            else zoomAxis = inputAxis;

            CameraTransform.Translate(ray.direction * zoomDistance * zoomAxis, Space.World);

            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            float v = CrossPlatformInputManager.GetAxis("Vertical");

            
            if (Input.GetMouseButton(1))
            {
                if (deltaFramePosition.x > 0.0f) h = 1.0f;
                else if (deltaFramePosition.x < 0.0f) h = -1.0f;
            }

            VectorMove = v * CharacterTransform.forward + h * CharacterTransform.right;
            if (Input.GetKey(KeyCode.LeftShift)) VectorMove *= 1.5f;
            Character.Move(VectorMove, false, IsJump);
            IsJump = false;

            this.LastFrameMousePosition = Input.mousePosition;
        }
    }
}
