﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialMover : MonoBehaviour {

    public Material MovedMaterial_0;
    public float MovedSpeed_0;

    void Update()
    {
        Vector2 offset = MovedMaterial_0.mainTextureOffset;
        offset.y -= MovedSpeed_0 * Time.deltaTime;
        if (offset.y <= 0.0f) offset.y = 10.0f;
        MovedMaterial_0.mainTextureOffset = offset;
    }
}
