﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationScript : MonoBehaviour {

    public GameObject Terrain { get { return GameObject.FindGameObjectWithTag("Terrain"); } }
    public Camera MainCamera { get { return GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>(); }}
    public GameObject Player { get { return GameObject.FindGameObjectWithTag("Player"); }}
    public MaterialLibraryScript Materials { get { return this.transform.Find("MaterialLibrary").gameObject.GetComponent<MaterialLibraryScript>(); }}
    public PrefabLibraryScript Prefabs { get { return this.transform.Find("PrefabLibrary").gameObject.GetComponent<PrefabLibraryScript>(); }}

    public GameObject SelectedObject { get; set; }
    public Vector3 HitNormal { get; set; }

    private void OnDrawGizmos()
    {
        for (int i = -100; i<100; i++)
        {
            Gizmos.DrawLine(
                new Vector3(-100.0f, .1f, (float)i + .5f),
                new Vector3(100.0f, .1f, (float)i + .5f)
                );
            Gizmos.DrawLine(
                new Vector3((float)i + .5f, .1f, -100.0f),
                new Vector3((float)i + .5f, .1f, 100.0f)
                );
        }
    }

    private void Update()
    {
        Ray ray = this.MainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if(Physics.Raycast(ray, out hit))
        {
            this.SelectedObject = hit.collider.gameObject;
            this.HitNormal = hit.normal;
        }
    }
}
