﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(Rigidbody))]
public class EGBuilding : EGObject {

	public Vector3 Position
    {
        get { return transform.position; }
        set { transform.position = value; }
    }
    public float Rotation
    {
        get { return transform.rotation.eulerAngles.y; }
        set { transform.rotation = Quaternion.Euler(0.0f, value, 0.0f); }
    }

    public EGEnums.BuildingDirections Direction { get; set; }
    public bool CollisionDetect { get; set; }

    public bool AllowBuildToTerrain;
    public bool AllowBuildToConstructions;
    public bool Builded;
    
    private void Awake()
    {
        if (!this.Builded)
        {
            this.Model.SetActive(false);
        }
    }

}
