﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabLibraryScript : MonoBehaviour {

    public enum ObjectType{
        SteelPillar_002 = 2,
        SteelPlatform_003 = 3
    }

    public GameObject Object002Pillar;
    public GameObject Object003Platform;

    public Dictionary<ObjectType, GameObject> Library { get; set; }

	private void Awake(){
        this.Library = new Dictionary<ObjectType, GameObject>();
        this.Library.Add(ObjectType.SteelPillar_002, this.Object002Pillar);
        this.Library.Add(ObjectType.SteelPlatform_003, this.Object003Platform);
	}
}
