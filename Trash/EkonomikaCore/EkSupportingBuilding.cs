﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EkSupportingBuilding : EkBuilding {
    public float InputHardness;
    public float ConsumedHardness;
    public float OutputHardness;

	internal void Update(){
        if (this.InputHardness > 0.0f) this.OutputHardness = this.InputHardness - this.ConsumedHardness;

        base.Update();
	}
}
