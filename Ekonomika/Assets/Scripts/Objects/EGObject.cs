﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EGObject : MonoBehaviour {

	public EGApplication App { get { return GameObject.FindGameObjectWithTag("Application").GetComponent<EGApplication>(); } }

    public string ObjectID;
    public GameObject Model;
    public string ObjectName;
    public string ObjectDescription;
}
