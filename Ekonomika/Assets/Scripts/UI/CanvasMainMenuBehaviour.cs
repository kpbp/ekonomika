﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasMainMenuBehaviour : MonoBehaviour {

    public EGApplication App { get { return GameObject.FindGameObjectWithTag("Application").GetComponent<EGApplication>(); } }

    public Button ButtonDestroy;
    public Button ButtonArmedPillar;
    public Button ButtonSteelPillar;
    public Button ButtonSupportBalk;
    public Button ButtonSupportPlatform;
    public Button ButtonSteelStair;
    public Text DiagnosticText;

	void Start () {
        ButtonDestroy.onClick.AddListener(delegate {
            App.PickupObject(App.Prefabs.Destroyer);
        });

        ButtonArmedPillar.onClick.AddListener(delegate{
            App.PickupObject(App.Prefabs.ArmedPillar001);
        });

        ButtonSteelPillar.onClick.AddListener(delegate {
            App.PickupObject(App.Prefabs.SteelPillar002);
        });

        ButtonSupportBalk.onClick.AddListener(delegate {
            App.PickupObject(App.Prefabs.SupportBalk003);
        });

        ButtonSupportPlatform.onClick.AddListener(delegate {
            App.PickupObject(App.Prefabs.SupportPlatform004);
        });

        ButtonSteelStair.onClick.AddListener(delegate {
            App.PickupObject(App.Prefabs.SteelStair005);
        });
    }
	
	// Update is called once per frame
	void Update () {
        
    }
}
