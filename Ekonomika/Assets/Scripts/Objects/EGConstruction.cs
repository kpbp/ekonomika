﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EGConstruction : EGBuilding {

    public int InputHardness;
    public int ConsumerHardness;
    public int OutputHardness;

    public bool AllowConnectToConstructions;
    public bool AllowEqualsConnection;
    public bool AllowInputBuild;
    public bool AllowInputConnect;
}
