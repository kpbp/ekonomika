﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour {

    public float TranslationSpeed = 8.0f;

    private Vector3 ClickMousePosition { get; set; }

    public Camera Camera { get { return Camera.main; } }

    private void Update(){
        //Зум по лучу
        Ray ray = Camera.ScreenPointToRay(new Vector2(Screen.width/2, Screen.height/2));
        float zoomDistance = 100f * Time.deltaTime;
        float zoomAxis = 0.0f;
        float inputAxis = Input.GetAxis("Mouse ScrollWheel");
        float altitude = Camera.transform.parent.position.y;

        if (altitude < 4.6f) zoomAxis = inputAxis < 0.0f ? inputAxis : 0.0f;
        else if (altitude > 10.7f) zoomAxis = inputAxis > 0.0f ? inputAxis : 0.0f;
        else zoomAxis = inputAxis;

        Camera.transform.parent.Translate(ray.direction * zoomDistance * zoomAxis, Space.World);

        //Вращение камеры
        if (Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2)) this.ClickMousePosition = Input.mousePosition;
        if (Input.GetMouseButtonUp(1) || Input.GetMouseButtonUp(2)) this.ClickMousePosition = Vector3.zero;

        Vector3 currentMousePosition = Input.mousePosition;
        Vector3 deltaPosition = this.ClickMousePosition - currentMousePosition;

        if (Input.GetMouseButton(1))
        {
            RaycastHit hit;
            float rotateAngle = 0.0f;
            Physics.Raycast(ray, out hit, Mathf.Infinity);

            if (deltaPosition.x > 1.0f) rotateAngle = deltaPosition.x / 5;
            else if (deltaPosition.x < -1.0f) rotateAngle = -1 * deltaPosition.x / 5;

            Camera.transform.RotateAround(hit.point, Vector3.up, rotateAngle*Time.deltaTime);
        }

        //Перемещения камеры

        if(Input.GetMouseButton(2)){
            Vector3 vectorRight = -1 * Camera.transform.right;
            vectorRight.y = 0.0f;

            Vector3 vectorForward = -1 * Camera.transform.forward;
            vectorForward.y = 0.0f;

            Vector3 translateVectorX = new Vector3();
            Vector3 translateVectorY = new Vector3();

            if(deltaPosition.x > 1.0f) translateVectorX = vectorRight * Time.deltaTime * TranslationSpeed;
            else if(deltaPosition.x < -1.0f) translateVectorX = -1 * vectorRight * Time.deltaTime * TranslationSpeed;

            if (deltaPosition.y > 1.0f) translateVectorY = vectorForward * Time.deltaTime * TranslationSpeed;
            else if (deltaPosition.y < -1.0f) translateVectorY = -1 * vectorForward * Time.deltaTime * TranslationSpeed;

            Camera.transform.parent.position += translateVectorX + translateVectorY;
        }
    }
}
