﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object000IndicatorArrowBehaviour : MonoBehaviour {

    public ApplicationScript App { get { return GameObject.FindGameObjectWithTag("Application").GetComponent<ApplicationScript>(); } }

    public GameObject Indicator;
    public SpriteRenderer Renderer { get { return this.Indicator.GetComponent<SpriteRenderer>(); }}

    public Sprite ArrowBlue;
    public Sprite ArrowYellow;

    public enum IndicatorType{ Sender, Network }
    public IndicatorType Type;

	private void Update(){
        switch(Type){
            case IndicatorType.Network:
                this.Renderer.sprite = ArrowYellow;
                break;
            case IndicatorType.Sender:
                this.Renderer.sprite = ArrowBlue;
                break;
        }
	}
}
