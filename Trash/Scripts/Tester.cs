﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tester : MonoBehaviour {

    public GameObject Marker;
    public GameObject Terrain { get { return GameObject.FindGameObjectWithTag("Terrain"); }}
    public Plane TerrainPlane { get; set; }

	private void Start()
	{
        this.TerrainPlane = new Plane(Terrain.transform.up, Terrain.transform.position);
	}

	private void Update()
	{
        float dist;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if(TerrainPlane.Raycast(ray, out dist)){
            Vector3 position = ray.GetPoint(dist);
            Marker.transform.position = new Vector3((float)(int)position.x, 0.0f, (float)(int)position.z);
        }

        if(Input.GetKeyDown(KeyCode.R)){
            Marker.transform.Rotate(Vector3.up, 90.0f);
        }

        /*RaycastHit hit;
        if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 9)){
            if(hit.collider.gameObject.tag == "Terrain"){
                Marker.transform.position = hit.point;
            }
        }*/
	}
}

/*public class Marker{
    public enum MarkerType
    {
        Common,
        Sender,
        Network

    }

    public MarkerType Type { get; set; }

    public Material Material {
        get{
            Material material;
            Tester tester = GameObject.FindGameObjectWithTag("Application").GetComponent<Tester>();

            switch(this.Type){
                case MarkerType.Common:
                    material = tester.GreenMaterial;
                    break;
                case MarkerType.Network:
                    material = tester.YellowMaterial;
                    break;
                case MarkerType.Sender:
                    material = tester.BlueMaterial;
                    break;
                default:
                    material = tester.RedMaterial;
                    break;
            }

            return material;
        }
    }

    public Vector3 Position { get; set; }

    public GameObject MarkerObject{
        get{
            GameObject gameObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
            gameObject.transform.localScale = new Vector3(.9f, 1.0f, .9f);
            gameObject.transform.position = this.Position;

            MeshRenderer renderer = gameObject.GetComponent<MeshRenderer>();
            renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            renderer.material = this.Material;

            return gameObject;
        }
    }

    public Marker(MarkerType markerType, Vector3 position){
        this.Type = markerType;
        this.Position = position;
    }
}*/
