﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorScript : MonoBehaviour {

    public GameObject line0object;
    public GameObject line1object;
    public GameObject line2object;

    public bool Reload;

    public float Speed;

    public float StartLineCoordinate{
        get { return this.Position.z - .5f; }
    }

    public float FinalLineCoordinate{
        get { return this.Position.z + .5f; }
    }

    public enum ItemDirectionEnum { North, East, South, West}

    public ItemDirectionEnum ItemDirection;

    private Quaternion Rotation{
        get { return transform.rotation; }
        set { transform.rotation = value; }
    }

    private Vector3 Position{
        get { return transform.position; }
        set { transform.position = value; }
    }

	private void Update(){
        float angle;

        if (this.ItemDirection == ItemDirectionEnum.North) angle = 0.0f;
        else if (this.ItemDirection == ItemDirectionEnum.East) angle = 90.0f;
        else if (this.ItemDirection == ItemDirectionEnum.South) angle = 180.0f;
        else angle = 270.0f;

        this.Rotation = Quaternion.Euler(new Vector3(0.0f, angle, 0.0f));

        if(this.Reload){
            line0object.transform.position = new Vector3(line0object.transform.position.x, line0object.transform.position.y, this.StartLineCoordinate);
            line1object.transform.position = new Vector3(line1object.transform.position.x, line1object.transform.position.y, this.StartLineCoordinate);
            line2object.transform.position = new Vector3(line2object.transform.position.x, line2object.transform.position.y, this.StartLineCoordinate);
            this.Reload = false;
        }

        if (line0object.transform.position.z < this.FinalLineCoordinate) line0object.transform.position += new Vector3(0.0f, 0.0f, this.Speed * Time.deltaTime);
        if (line1object.transform.position.z < this.FinalLineCoordinate) line1object.transform.position += new Vector3(0.0f, 0.0f, this.Speed * Time.deltaTime);
        if (line2object.transform.position.z < this.FinalLineCoordinate) line2object.transform.position += new Vector3(0.0f, 0.0f, this.Speed * Time.deltaTime);
	}
}
