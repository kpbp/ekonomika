﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(BoxCollider))]
public class EkBuilding : EkObject {
    public enum ObjectLayer { Uninstall = 11, Install = 10 }
    public enum ObjectDirection { North = 0, East = 1, South = 2, West = 3 }

    public PrefabLibraryScript.ObjectType ObjectID;
    public Vector3 ColliderSize;
    public ObjectLayer Layer;
    public float PositionOffset;
    public bool AllowMultistage;

    private float ObjectLevel { get; set; }
    private Vector3 Position{
        get { return this.transform.position; }
        set{
            Vector3 oldPosition = this.transform.position;
            Vector3 newPosition = value;

            if(oldPosition.x != newPosition.x || oldPosition.z != newPosition.z) OnPositionChange();

            this.transform.position = newPosition;
        }
    }

    internal Rigidbody Rigidbody { get { return this.GetComponent<Rigidbody>(); } }
    internal BoxCollider BoxCollider { get { return this.GetComponent<BoxCollider>(); } }

    internal ObjectDirection Direction { get; set; }

    internal float DistanceToPlayer { get { return (this.Position - App.Player.transform.position).magnitude; } }

    internal GameObject Model { get { return this.transform.Find("model").gameObject; } }
    internal GameObject Marker { get; set; }

    internal bool IsInstalled { get; set; }
    internal bool IsCollision { get; set; }
    internal bool IsFar { get; set; }
    internal bool IsGrounded { get; set; }

    internal void ChangeObjectMaterial(GameObject gameObject, Material material)
    {
        MeshRenderer renderer = gameObject.GetComponent<MeshRenderer>();
        if (renderer != null)
        {
            renderer.materials = new Material[] { };
            renderer.material = material;
        }
        for (int i = 0; i < gameObject.transform.childCount; i++) this.ChangeObjectMaterial(gameObject.transform.GetChild(i).gameObject, material);
    }

    internal void CreateMarker(){
        this.Marker = Instantiate(this.Model);
        this.Marker.transform.SetParent(this.transform);
        this.ChangeObjectMaterial(this.Marker, App.Materials.MarkerGreen);
    }

	internal void Awake(){
        this.Rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        this.BoxCollider.size = this.ColliderSize*.9f;
        this.BoxCollider.center = new Vector3(0.0f, this.ColliderSize.y * .9f / 2, 0.0f);

        this.ObjectLevel = 0.0f;

        this.Model.SetActive(false);
        this.CreateMarker();
	}

    internal void Install()
    {
        this.IsInstalled = true;
        this.Layer = ObjectLayer.Install;
    }

    internal void OnCollisionEnter(Collision collision)
    {
        if (!IsInstalled)
        {
            GameObject collisionObject = collision.collider.gameObject;
            EkBuilding collisionBuilding = collisionObject.GetComponent<EkBuilding>();
            if(collisionBuilding != null && collisionBuilding.AllowMultistage){
                this.ObjectLevel++;
            }else{
                this.IsCollision = true;
            }
        }
    }

    internal void OnCollisionExit(Collision collision)
    {
        if (!IsInstalled)
        {
            this.IsCollision = false;
        }
    }

    internal void OnPositionChange(){
        this.ObjectLevel = 0.0f;
    }

    internal void Update()
    {
        this.gameObject.layer = (int)this.Layer;

        if (!IsInstalled)
        {
            this.Model.SetActive(false);
            this.Marker.SetActive(true);

            float dist;
            Plane plane = new Plane(App.Terrain.transform.up, App.Terrain.transform.position);

            Ray ray = App.MainCamera.ScreenPointToRay(Input.mousePosition);
            if (plane.Raycast(ray, out dist))
            {
                Vector3 position = ray.GetPoint(dist);
                if (this.Direction == ObjectDirection.East || this.Direction == ObjectDirection.West)
                    this.Position = new Vector3((float)(int)position.x + this.PositionOffset, this.ObjectLevel, (float)(int)position.z);
                else
                    this.Position = new Vector3((float)(int)position.x, this.ObjectLevel, (float)(int)position.z + this.PositionOffset);
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                //this.transform.Rotate(Vector3.up, 90.0f);
                int enumLength = System.Enum.GetValues(typeof(ObjectDirection)).Length;
                int nextIndex = (int)this.Direction + 1;

                if (nextIndex >= enumLength) nextIndex = 0;

                this.Direction = (ObjectDirection)nextIndex;
            }

            this.transform.rotation = Quaternion.Euler(0.0f, 90.0f * (float)this.Direction, 0.0f);

            this.IsFar = this.DistanceToPlayer > 7.0f ? true : false;

            if (IsFar)
            {
                this.ChangeObjectMaterial(this.Marker, App.Materials.MarkerBlack);
            }
            else
            {
                if (IsCollision)
                {
                    this.ChangeObjectMaterial(this.Marker, App.Materials.MarkerRed);
                }
                else
                {
                    this.ChangeObjectMaterial(this.Marker, App.Materials.MarkerGreen);
                    if (Input.GetMouseButtonDown(0))
                    {
                        this.Install();
                        if (Input.GetKey(KeyCode.LeftShift))
                        {
                            Instantiate(App.Prefabs.Library[this.ObjectID]);
                        }
                    }
                }
            }

        }
        else
        {
            this.Model.SetActive(true);
            this.Marker.SetActive(false);
        }
    }
}
