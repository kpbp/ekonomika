﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object002PillarBehaviour : EkBuilding {
    void Awake(){
        this.PositionOffset = 0.0f;
        this.Layer = ObjectLayer.Uninstall;
        this.ColliderSize = new Vector3(0.9f, 0.9f, 0.9f);
        base.Awake();
	}

    void Update(){
        base.Update();    
    }
}
